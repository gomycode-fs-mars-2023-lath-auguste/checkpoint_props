import React from 'react';
import PropTypes from 'prop-types';
import Button from 'react-bootstrap/Button';
import '../Profile/profile.css'

const Profile = ({fullName, bio, profession, children}) => {
    // const  = props

    const handleName =(e) => {
        e.preventDefault();
        alert(fullName)
    }
    return (
        <div>
            <div className="container carte ">
                <div className="row bg-white py-5">
                    <div className="col-4 box-gauche ">
                        <img src={children} alt="Profile" className='w-100 h-50'  />
                        <div className='mt-5 box-bio'>
                            <h3>About Me </h3>
                            <p>{bio}</p>
                        </div>
                    </div>

                    <div className="col box-droit">
                        <div className='box-entete'>
                            <h1 className='' ><span className=''>HELLO</span> </h1>
                            <h2>I'm {fullName}</h2>
                            <p className='metier'>{profession}</p>
                        </div>

                        <div className='containerFluid pt-3'>
                            <div className="row">
                                <div className="col-4 fw-bold">
                                    <p>Age</p>
                                    <p>Address</p>
                                    <p>E-mail</p>
                                    <p>Phone</p>
                                </div>
                                <div className="col">
                                    <p>25</p>
                                    <p>08 pb 5747 Yakro 08</p>
                                    <p>test@gmail</p>
                                    <p>01-234-567-89</p>
                                </div>
                            </div>
                            <div className=''>
                                <Button variant="warning text-white mt-3 " size="lg" onClick={handleName}>Check the Fullname!</Button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            {/* Footer */}
            <div className='footer carte d-flex justify-content-center align-items-center text-center py-4'>
                <div className='d-flex w-50 justify-content-between align-items-center'>
                    <i class="fa-brands fa-facebook-f"></i>
                    <i class="fa-brands fa-twitter"></i>
                    <i class="fa-brands fa-instagram"></i>
                    <i class="fa-brands fa-linkedin-in"></i>
                </div>
            </div>
            
        </div>
    );
};

Profile.propTypes = {
    fullName: PropTypes.string.isRequired,
    bio: PropTypes.string.isRequired,
    profession: PropTypes.string.isRequired,
    children: PropTypes.string.isRequired,
    handleName: PropTypes.func.isRequired,
    };

Profile.defaultProps = {
    fullName: 'John Doe',
    bio: 'No bio available',
    profession: 'Unknown',
    children: '',
    handleName: () => {},
};

export default Profile;