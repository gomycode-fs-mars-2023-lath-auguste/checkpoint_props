import React from 'react';
import Profile from './Profile/Profile';
import 'bootstrap/dist/css/bootstrap.min.css';

const App = () => {


  const myListe = [
    {
      fullName: 'Pierre Balmon',
      bio: "Lorem ipsum, dolor sit amet consectetur adipisicing.",
      profession: 'Teacher',
      children: '../public/image/img4.jpg'
    },
    {
      fullName: 'Pierre Balmon',
      bio: "Lorem ipsum, dolor sit amet consectetur adipisicing.",
      profession: 'Teacher',
      children: '../public/image/img4.jpg'
    }
  ]

  return (
    <div>
      <h1 className='display-5 fw-bold text-center'>Welcome</h1>
      <div className="container">
        <div className="row pt-5 d-flex justify-content-center align-items-center ">
          <div className="col-6 ">
            <Profile 
            fullName='Pierre Balmon' 
            bio='Lorem ipsum dolor sit amet consectetur adipisicing elit. Pariatur quaerat cum nostrum maiores numquam ea voluptates voluptatibus, nemo facere, dignissimos doloremque! Dicta, sunt? Quibusdam, rem!
            amet consectetur adipisicing elit. Pariatur quaerat cum nostrum maiores numquam ea voluptates voluptatibus, nemo facere, dignissimos doloremque! Dicta, sunt? Quibusdam, rem!'  
            profession="Teacher">
              /image/img7.jpg
            </Profile>
          </div>
        </div>
      </div>
    </div>
  );
};

export default App;